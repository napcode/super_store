/// Support for doing something awesome.
///
/// More dartdocs go here.
library;

import 'dart:async';

class SuperStoreManager {
  final List<SuperStoreSection> sections;
  SuperStoreManager(this.sections);
  void enableLogs() {
    listenToAll((section, event) {
      final json = event.toJson();
      json['section'] = section.sectionName;
      print(json);
    });
  }

  void listenToAll(Function(SuperStoreSection, Event) f) {
    for (var section in sections) {
      section.listen((event) => f(section, event));
    }
  }
}

class SuperStoreSection {
  final sectionName = 'name';
  final events = EventBus();

  void send(Event event) {
    events.send(event);
  }

  StreamSubscription listen(Function(Event) f) {
    return events.stream.listen((event) => f(event));
  }
}

class Event {
  final eventName = "event";

  Map<String, dynamic> toJson() => {
        'eventName': eventName,
      };

  @override
  String toString() {
    return eventName;
  }
}

class EventBus {
  var controller = StreamController<Event>.broadcast();
  Stream get stream {
    return controller.stream;
  }

  Sink get sink {
    return controller.sink;
  }

  void send(Event event) {
    sink.add(event);
  }
}
