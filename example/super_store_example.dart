import 'package:super_store/super_store.dart';

class MainSection extends SuperStoreSection {
  @override
  get sectionName => "main";
  var exampleEventFired = false;
}

class Store {
  static final main = MainSection();
  static final manager = SuperStoreManager([main]);
}

class ExampleEvent extends Event {
  @override
  get eventName => 'example';
}

void main() {
  Store.manager.enableLogs();
  Store.main.listen((event) {
    if (event is ExampleEvent) {
      Store.main.exampleEventFired = true;
    }
  });

  Store.main.send(ExampleEvent());
}
