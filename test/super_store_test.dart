import 'package:super_store/super_store.dart';
import 'package:test/test.dart';

class MainSection extends SuperStoreSection {
  @override
  get sectionName => 'main';
  var exampleEventFired = false;
}

class Store {
  static final main = MainSection();
  static final manager = SuperStoreManager([main]);
}

class ExampleEvent extends Event {
  @override
  get eventName => 'example';
}

void main() {
  group('All Tests', () {
    setUp(() {
      Store.main.listen((event) {
        if (event is ExampleEvent) {
          Store.main.exampleEventFired = true;
        }
      });
    });
    test('Test event', () async {
      Store.main.send(ExampleEvent());
      await Future.delayed(Duration(milliseconds: 1));
      expect(Store.main.exampleEventFired, isTrue);
    });
  });
}
